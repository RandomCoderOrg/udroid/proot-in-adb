#!/system/bin/sh

unset LD_PRELOAD

ROOT="/data/local/tmp" # /data/local/tmp is avalible to RW and also mounted with exec
fsroot="/data/local/tmp/$1"

## ENV variables
LIB="$ROOT/lib/$(getprop ro.product.cpu.abi)"
LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$LIB"
BUSYBOX="$ROOT/busybox/$(getprop ro.product.cpu.abi)/bin/busybox"
proot="$ROOT/proot/$(getprop ro.product.cpu.abi)/proot"

$proot \
    -b /vendor \
    -b /system \
    -b /data/data/com.termux/files/usr \
    -b /linkerconfig/ld.config.txt \
    -b /storage/self/primary:/sdcard \
    -b /storage \
    -b /data/dalvik-cache \
    -b "${fsroot}"/tmp:/dev/shm \
    -b /proc/vmstat:/proc/vmstat \
    -b /proc/version:/proc/version \
    -b /proc/uptime:/proc/uptime \
    -b /proc/stat:/proc/stat \
    -b /proc/loadavg:/proc/loadavg \
    -b /sys \
    -b /proc/self/fd/2:/dev/stderr \
    -b /proc/self/fd/1:/dev/stdout \
    -b /proc/self/fd/0:/dev/stdin \
    -b /proc/self/fd:/dev/fd \
    -b /proc \
    -b /dev/urandom:/dev/random \
    -b /dev \
    --root-id \
    --cwd=/root -L \
    --sysvipc \
    --link2symlink \
    --kill-on-exit \
    --rootfs=${fsroot} \
    -w /root \
       /usr/bin/env -i \
       HOME=/root \
       PATH=/usr/local/sbin:/usr/local/bin:/bin:/usr/bin:/sbin:/usr/sbin \
       TERM=xterm-256color \
       LANG=C.UTF-8 \
        /bin/bash --login

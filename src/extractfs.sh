#!/system/bin/sh

unset LD_PRELOAD

ROOT="/data/local/tmp" # /data/local/tmp is avalible to RW and also mounted with exec

## ENV VARIABLES
LIB="$ROOT/lib/$(getprop ro.product.cpu.abi)"
LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$LIB"
BUSYBOX="$ROOT/busybox/$(getprop ro.product.cpu.abi)/bin/busybox"
proot="$ROOT/proot/$(getprop ro.product.cpu.abi)/proot"

if [ ! -f /data/local/tmp/filesystem ]; then
    mkdir filesystem
fi

echo "extracting tarball [\" $1 \"].."
sleep 5
$proot \
    --link2symlink \
    $BUSYBOX tar -xvJf "$1" -C "$ROOT/filesystem" || {
        echo "Something went wrong.."
    }

echo "Done.."
